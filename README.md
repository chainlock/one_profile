
# OneProfile :bust_in_silhouette:
OneProfile is a unique social network where users post their profiles into the blockchain in the form of NFTs. It's a new way to connect, share project and ideas, and discover  exciting opportunities .

## Description :page_facing_up:
In OneProfile, users can share their past projects, future plans, or interesting concepts and ideas they've come across. The goal is to create a platform for meaningful connections and collaboration.

Investors & Entrepreneurs can browse through these profiles and directly contact the ones they are interested in using Aezichat, email, or through blockscan. It's a new approach to investments and recrutement that leverages the transparency and security of blockchain technology.

## How it Works :gear:
1. **Post your profile**: Create your profile and post it as an NFT on the blockchain.
2. **Share your ideas**: Talk about your  projects, future plans, or any interesting concepts you've discovered.
3. **Connect**: Find other profiles that catch your interest and start a conversation via Aezichat, email, or blockscan.

## Current Status :construction:
Currently, we have the front end of OneProfile ongoing using the framework [SolidJs](https://solidjs.com).

In the project directory, you can run:

```bash
$ yarn install # or pnpm install or npm install
```

### `yarn dev`

Runs the app in the development mode.<br>
Open [http://localhost:5173](http://localhost:5173) to view it in the browser.

### `yarn build`

Builds the app for production to the `dist` folder.<br>
It correctly bundles Solid in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!


## License :scroll:
This project is licensed under the MIT License. See the `LICENSE.md` file for details.

## Contact :mailbox:
For any inquiries or feedback, feel free to reach out to us at (Aezichain@proton.me).


Visit our official website: [Aezichain](https://aezichain-chainlock-71b2e586f44f19b7368213b25767dec3cf69bd92a75.gitlab.io/)