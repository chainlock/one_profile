import { createSignal, onCleanup } from 'solid-js';

function Work() {
  const [activeWindow, setActiveWindow] = createSignal('overview');
  const [isInputVisible, setInputVisible] = createSignal(false);

  return (
    <div class='work'>
      <div class='work-navigation'>
        <button class='button-overview' onClick={() => setActiveWindow('overview')}>
          Overview
        </button>
        <button class='button-write' onClick={() => setActiveWindow('write')}>
          Write
        </button>
        <button class='button-sign' onClick={() => setActiveWindow('sign')}>
          Sign
        </button>
        <button
          class='button-share'
          onClick={() => {
            setActiveWindow('share');
            setInputVisible(true);
          }}
        >
          Share
        </button>
      </div>
      <div class='content'>
        {activeWindow() === 'overview' && <Overview />}
        {activeWindow() === 'write' && <Write />}
        {activeWindow() === 'sign' && <Sign />}
        {activeWindow() === 'share' && <Share />}
      </div>
    </div>
  );
}

function Overview() {
  const [text, setText] = createSignal('');

  return (
    <div class='overview-content'>
      <textarea
        value={text()}
        onInput={(e) => setText(e.target.value)}
        placeholder='Overview content goes here'
        style={{
          background: 'none',
          color: 'whitesmoke',
          width: '100%',
          height: '100%',
          border: '2px solid white',
          resize: 'none',
        }}
      />
      <button
        class='button-overview'
        type='submit'
        style={{
          width: '100%',
        }}
      >
        Save
      </button>
    </div>
  );
}

function Write() {
  const [text, setText] = createSignal('');

  return (
    <div class='write-content'>
      <textarea
        value={text()}
        onInput={(e) => setText(e.target.value)}
        placeholder='Write Content goes here'
        style={{
          background: 'none',
          color: 'whitesmoke',
          width: '100%',
          height: '100%',
          border: '2px solid white',
          resize: 'none',
        }}
      />
      <button
        class='button-write'
        type='submit'
        style={{
          width: '100%',
        }}
      >
        Save
      </button>
    </div>
  );
}

function Sign() {
  const [input, setInput] = createSignal('');
  const [captcha, setCaptcha] = createSignal(() =>
    Math.floor(Math.random() * 8999 + 1000)
  );

  const validateCaptcha = () => {
    if (input() === captcha().toString()) {
      alert('Captcha matched');
    } else {
      alert('Captcha does not match');
      setCaptcha(() => Math.floor(Math.random() * 8999 + 1000));
    }
  };

  return (
    <div class='sign-content'>
      <p>{captcha()}</p>
      <input
        value={input()}
        onInput={(e) => setInput(e.target.value)}
        placeholder='Enter the captcha...'
      />
      <button onClick={validateCaptcha}>Submit</button>
    </div>
  );
}

function Share() {
  const handleShare = () => {
    alert('Content shared!');
  };

  return (
    <div class='share-content'>
      <p>Content from previous buttons goes here.</p>
      <button onClick={handleShare}>Share</button>
    </div>
  );
}

export default Work;