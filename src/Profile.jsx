import { createSignal } from 'solid-js';
import { styled } from 'solid-styled-components';

const Container = styled('div')`
  color: #FFC107;
  text-align: center;
`;

const ProfilePicture = styled('img')`
  margin-bottom: 20px;
  width: 160px;
  height: 160px;
  object-fit: cover;
  border-radius: 50%;
  border: 2px solid #c49802;
`;

const Description = styled('div')`
  margin: 10px;
  padding: 10px;
  background-color: #0a0a23;
  color: #ffffff;
  border: 2px solid #c49802;
  height: 150px; /* Set the height to match CreationDescription */
  font-size: 18px;
`;

const ProfileName = styled('h2')`
  font-size: 24px;
  margin: 10px;
`;

const EditButton = styled('button')`
  background-color: #FFC107;
  border: 2px solid #000;
  color: #000;
  padding: 5px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 10px;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #000;
    color: #FFC107;
  }
`;

const CreationButton = styled('button')`
  background-color: #c49802;
  border: 2px solid #0a0a23;
  color: #fff;
  padding: 5px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 10px;
  cursor: pointer;

  &:hover {
    background-color: #FFC107;
  }
`;

const CreationDescription = styled('div')`
  background-color: #0a0a23;
  border: 2px solid #c49802;
  color: #fff;
  padding: 10px;
  text-align: center;
  font-size: 16px;
  margin: 0px;
`;

const Profile = () => {
  const [state, setState] = createSignal({
    name: 'Your Name',
    description: 'Add a description.',
    creationDescription: 'Click here to add a creation description.',
    activeField: null,
  });

  const [creations] = createSignal([
    { name: 'Creation 1', description: 'Description of Creation 1' },
    { name: 'Creation 2', description: 'Description of Creation 2' },
    { name: 'Creation 3', description: 'Description of Creation 3' },
    // Add more creations as needed
  ]);

  const showField = (field) => () => {
    setState((prev) => ({
      ...prev,
      activeField: field,
    }));
  };

  const handleInputChange = (field, value) => {
    setState((prev) => ({
      ...prev,
      [field]: value,
    }));
  };

  return (
    <Container>
      <ProfilePicture src="profile.png" alt="Profile Picture" />
      <ProfileName>{state().name}</ProfileName>
      <div>
        <EditButton onClick={showField('description')}>Description</EditButton>
        <EditButton onClick={showField('creationDescription')}> Creation </EditButton>
      </div>

      {state().activeField === 'description' ? (
        <Description>{state().description}</Description>
      ) : null}

      {state().activeField === 'creationDescription' ? (
        <>
          {creations().map((creation) => (
            <div key={creation.name}>
              <CreationButton onClick={() => handleInputChange('creationDescription', creation.name)}>
                {creation.name}
              </CreationButton>
              {state().creationDescription === creation.name && (
                <CreationDescription>{creation.description}</CreationDescription>
              )}
            </div>
          ))}
        </>
      ) : null}
    </Container>
  );
};

export default Profile;
