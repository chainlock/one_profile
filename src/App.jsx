
import { createSignal } from 'solid-js';
import '/style.css';
import Profile from './Profile';
import Explore from './Explore';
import Work from './Work';

function App() {
  const [component, setComponent] = createSignal('profile');

  const handleExploreClick = () => setComponent('explore');
  const handleProfileClick = () => setComponent('profile');
  const handleWorkClick = () => setComponent('work');

  const handleNewUserMessage = (newMessage) => {
    console.log(`New message incoming! ${newMessage}`);
    // Now send the message through the backend API
  };

  return (
    <>
      <header style={{ color: '#FFC107', textAlign: 'center' }}>
        <figure>
          <img src="logo.png" className="logo" alt="Website Logo" />
        </figure>
        <nav style={{ padding: '0', margin: '5px' }}>
          <button className="explore-button" onClick={handleExploreClick}>
            Explore
          </button>
          <button className="profile-button" onClick={handleProfileClick}>
            Profile
          </button>
          <button className="work-button" onClick={handleWorkClick}>
              Create
          </button>
        </nav>
      </header>
      {component() === 'profile' ? (
        <Profile />
      ) : component() === 'explore' ? (
        <Explore />
      ) : (
        <Work />
      )}
      {/* Widget component goes here */}
    </>
  );
}

export default App;