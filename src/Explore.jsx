import { createSignal } from 'solid-js';
import { styled } from 'solid-styled-components';

const Container = styled('div')`
  color: #FFC107;
  text-align: center;
`;

const Input = styled('input')`
  margin: 10px;
  padding: 10px;
  background-color: #0a0a23;
  color: #ffffff;
  border: none;

  &:focus {
    outline: none;
  }
`;

const SubmitButton = styled('input')`
  background-color: #0a0a23;
  border: 2px solid #c49802;
  color: #fff;
  padding: 5px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 0px;
  cursor: pointer;

  &:hover {
    background-color: #c49802;
  }
`;

const CreationButton = styled('button')`
  background-color: #c49802; /* Different color for creation buttons */
  border: 2px solid #0a0a23; /* Different border for creation buttons */
  color: #fff;
  padding: 5px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 10px;
  cursor: pointer;

  &:hover {
    background-color: #FFC107;
  }
`;

const CreationDescription = styled('div')`
  background-color: #0a0a23;
  border: 2px solid #c49802;
  color: #fff;
  padding: 10px;
  text-align: center;
  font-size: 16px;
  margin: 0px;
`;

const Explore = () => {
  const [searchTerm, setSearchTerm] = createSignal('');
  const [showCreationDescription, setShowCreationDescription] = createSignal(false);
  const [selectedCreation, setSelectedCreation] = createSignal('');

  const handleSearch = (event) => {
    event.preventDefault();
    console.log(`Searching for ${searchTerm()}`);
  };

  const handleCreationClick = (creation) => {
    setSelectedCreation(creation);
    setShowCreationDescription(true);
  };

  return (
    <Container>
      <form onSubmit={handleSearch}>
        <Input
          type="text"
          placeholder="Search for users"
          value={searchTerm()}
          onInput={(e) => setSearchTerm(e.target.value)}
        />
        <SubmitButton type="submit" value="Search" />
      </form>
      <div className="explore">
        <div className="creations">
          <div className="creation">
            <CreationButton onClick={() => handleCreationClick('Creation 1')}>Creation 1</CreationButton>
            {showCreationDescription() && selectedCreation() === 'Creation 1' && (
              <CreationDescription>Description of Creation 1...</CreationDescription>
            )}
          </div>
          <div className="creation">
            <CreationButton onClick={() => handleCreationClick('Creation 2')}>Creation 2</CreationButton>
            {showCreationDescription() && selectedCreation() === 'Creation 2' && (
              <CreationDescription>Description of Creation 2...</CreationDescription>
            )}
          </div>
          <div className="creation">
            <CreationButton onClick={() => handleCreationClick('Creation 3')}>Creation 3</CreationButton>
            {showCreationDescription() && selectedCreation() === 'Creation 3' && (
              <CreationDescription>Description of Creation 3...</CreationDescription>
            )}
          </div>
          <div className="creation">
            <CreationButton onClick={() => handleCreationClick('Creation 4')}>Creation 4</CreationButton>
            {showCreationDescription() && selectedCreation() === 'Creation 4' && (
              <CreationDescription>Description of Creation 4...</CreationDescription>
            )}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Explore;
