/* @refresh reload */
import { render } from "solid-js/web";
import '/style.css';
import App from './App';
import Explore from './Explore';
import Work from './Work';
import Profile from './Profile';
import { Route, Router, Routes } from '@solidjs/router';

const root = document.getElementById('root')

render(() => <Router> 
    <Routes>
    <Route path="/" element={<App />} />
    <Route path="/explore" element={<Explore />} />
    <Route path="/profile" element={<Profile />} />
    <Route path="/work" element={<Work />} />
    </Routes>
  </Router>, root);